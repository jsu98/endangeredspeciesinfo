FROM node:8 as react-build
WORKDIR /app

#COPY package.json /app/package.json
COPY . ./
RUN npm install --silent
RUN npm audit fix
RUN npm install typescript --silent
RUN npm install react-scripts@3.0.1 --silent

EXPOSE 3000
CMD ["npm", "start"]
