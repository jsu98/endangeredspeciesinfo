# This Python file uses the following encoding: utf-8
from datetime import datetime
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from sqlalchemy.ext.declarative import declarative_base
import psycopg2
import requests
import random
from models import Animal, Country, Area, db, app, engine
import json

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

REDLIST_TOKEN = "9bb4facb6d23f48efbf424bb05c0c1ef1cf6f468393bc745d42179ac4aca5fee"
REDLIST_BASEURL = "https://apiv3.iucnredlist.org/api/v3/"
AIRQUALITY_TOKEN = "a455ac52659b9227aa1a3e83e8b61eb85a649f10"
PROTECTED_TOKEN = "58c62aed4fa4e57448d9c9033b84c5a1"
BING_API_KEY1 = '165861daebb14830bae423d3ac733da9'
BING_API_KEY2 = '77a655f9ac5c4922a65330467467cccd'
country_data = {}
animal_data = {}
area_data = {}
protectedAreas = []
areaNames = set()

# get list of countries
def get_list_countries():
    # the region name
    global country_data
    global protectedAreas
    regions_to_get = {"SAS", "ECS", "EAS", "NAC"}
    parameters = {"format": "json"}
    response = requests.get(
        "http://api.worldbank.org/v2/country?", params=parameters
    ).json()
    region_count = {}
    # print(len(response[1]))

    num_pages = response[0]["pages"]

    # iterate through pages
    for i in range(1, num_pages):
        parameters = {"format": "json", "page":i}
        response = requests.get(
            "http://api.worldbank.org/v2/country?", params=parameters
        ).json()
        limit_per_page = 10
        count = 0
        print("on country api page: ", i)
        # get country data
        for i in range(0, len(response[1]), 1):
            dic = response[1][i]
            region_id = dic["region"]["id"]
            region_name = dic["region"]["value"]

            iso = dic["id"]
            iso2 = dic["iso2Code"]
            cap_city = dic["capitalCity"]
            lon = dic["longitude"]
            lat = dic["latitude"]
            name = dic["name"]

            create_new_dict = False

            # only want to get countries from specific regions
            if region_id in regions_to_get:
                # only want 10 countries per region
                if region_id in region_count.keys():
                    if region_count[region_id] < 20:
                        create_new_dict = len(cap_city) > 1
                        if create_new_dict:
                            region_count[region_id] += 1
                            count += 1
                else:
                    create_new_dict = len(cap_city) > 1
                    if create_new_dict:
                        region_count[region_id] = 1
                        count += 1

            if create_new_dict:

                aqi, pm10 = get_air_data(lat, lon)
                num_areas = get_an_area(iso)
                get_animals(iso2, name)

                # and add this to country_data
                new_dic = {
                    "name": name,
                    "countryId": iso,
                    "capitalCity": cap_city,
                    "regionId": region_id,
                    "regionName": region_name,
                    "longitude": lon,
                    "latitude": lat,
                    "aqi": aqi,
                    "pm10": pm10,
                    "areas": num_areas,
                }
                country_data[iso] = new_dic
            if count == limit_per_page:
                count = 0
                break


# populates the area_data dictionary, and returns num_areas for a specific country
def get_an_area(iso):
    global area_data
    # country's iso3 id, only way to search prot areas by country
    areaResponse = requests.get(
        "http://api.protectedplanet.net/v3/protected_areas/search?token="
        + PROTECTED_TOKEN
        + "&country="
        + iso
    ).json()

    # areaResponse["protected_areas"] is the list of prot areas returned, save all of these to a global list, to save time for prot area part
    # no dupes, checks for name
    # areaList = areaResponse["protected_areas"]
    # print(areaResponse)
    num_areas = len(areaResponse["protected_areas"])

    if len(areaResponse["protected_areas"]) > 10:
        limit = 10
    else:
        limit = len(areaResponse["protected_areas"])

    count = 0
    # get 10 areas per country
    for i in areaResponse["protected_areas"]:
        ind = random.randint(0, len(areaResponse["protected_areas"]))
        name = i["name"]
        country = i["countries"][0]["name"]  # do we just want 1-1 mapping?
        area = i["reported_area"]
        # used to make a second call to enable geometry, couldn't find this info on country specific call
        wdpa_id = i["wdpa_id"]
        lastLegalUpdate = i["legal_status_updated_at"]
        new_dict = {
            "name": name,
            "wdpaId": wdpa_id,
            "country": country,
            "area": area,
            "lastLegalUpdated": lastLegalUpdate,
        }
        area_data[wdpa_id] = new_dict
        count += 1
        if count == limit:
            break

    return num_areas


def get_air_data(lat, lon):
    airResponse = requests.get(
        "https://api.waqi.info/feed/geo:"
        + lat
        + ";"
        + lon
        + "/?token="
        + AIRQUALITY_TOKEN
    ).json()
    # print(airResponse)

    airData = airResponse["data"]

    try:
        aqi = airData["aqi"]
    except:
        aqi = -1
        pm10 = -1

    try:
        iaqi = airData["iaqi"]
        pm10 = iaqi["pm10"]["v"]
    except:
        pm10 = -1
    return aqi, pm10


# format of country_data: {'name': name, 'countryId': country_id, 'capitalCity': cap_city, 'regionId': region_id, 'regionName': region_name, 'longitude': lon, 'latitude': lat}
# format of animal_data: new_dic = {'scientificName': name, 'name': name, 'taxonId': taxonId, 'country': country, 'category': category, 'habitat': habitat, 'population': population}
def get_animals(country_id, country_name):
    global REDLIST_BASEURL
    global REDLIST_TOKEN
    global country_data

    parameters = {"token": REDLIST_TOKEN}

    response = requests.get(
        "http://apiv3.iucnredlist.org/api/v3/country/getspecies/" + country_id,
        params=parameters,
    ).json()

    # gets 2 animals per country
    limit = len(response["result"]) - 1
    for i in range(2):
        ind = random.randint(0, limit)
        dic = response["result"][ind]
        taxon_id = dic["taxonid"]
        sci_name = dic["scientific_name"]
        status = dic["category"]

        # to get the main_common_name and population_trend
        species_response = requests.get(
            "http://apiv3.iucnredlist.org/api/v3/species/id/" + str(taxon_id),
            params=parameters,
        ).json()

        dic2 = species_response["result"][0]
        name = dic2["main_common_name"]
        population = dic2["population_trend"]
        # get habitat
        habitat_response = requests.get(
            "http://apiv3.iucnredlist.org/api/v3/habitats/species/id/" + str(taxon_id),
            params=parameters,
        ).json()

        # some habitats are empty, need to act accordingly
        if len(habitat_response["result"]) > 0:
            dic3 = habitat_response["result"][0]
            habitat = dic3["habitat"]
        else:
            habitat = ""
        # create dictionary for animal
        new_dic = {
            "taxonId": taxon_id,
            "scientific_name": sci_name,
            "name": name,
            "countryId": country_id,
            "countryName": country_name,
            "category": status,
            "habitat": habitat,
            "population": population,
        }
        animal_data[taxon_id] = new_dic


def populate_animals_table():
    print("populating animals table...")
    s = scoped_session(Session)
    for key, val in animal_data.items():
        taxonId = val["taxonId"]
        name = val["name"]
        species = val["scientific_name"]
        habitat = val["habitat"]
        country_name = val["countryName"]
        country_obj = Country.query.filter_by(name=country_name).first()
        try:
            session_country = Session.object_session(country_obj)
            if session_country is not None:
                session_country.close()
        except:
            pass
        # print("country obj: ", country_obj)
        pop = val["population"]
        category = val["category"]
        curr_animal = Animal(
            taxonId=taxonId,
            name=name,
            species=species,
            habitat=habitat,
            country=country_name,
            population=pop,
            status=category,
        )
        if Animal.query.filter_by(taxonId=taxonId).count() == 0:
            s.add(curr_animal)
            s.commit()
            country_obj.ecologies.append(curr_animal)
            s.commit()
    s.close()


def populate_area_table():
    print("populating area table...")
    s = scoped_session(Session)
    for key, val in area_data.items():
        name = val["name"]
        country_name = val["country"]
        wdpa_id = val["wdpaId"]
        country_obj = Country.query.filter_by(name=country_name).first()
        try:
            session_country = Session.object_session(country_obj)
            if session_country is not None:
                session_country.close()
        except:
            pass
        area = val["area"]
        update = val["lastLegalUpdated"]
        # avoids adding areas of the same name
        if Area.query.filter_by(name=name).count() == 0:
            curr_area = Area(
                wdpa_id=wdpa_id,
                name=name,
                country=country_name,
                area=float(area),
                lastUpdated=update,
                location=country_obj,
            )
            s.add(curr_area)
            s.commit()
    s.close()


def populate_country_table():
    print("populating country table...")
    s = scoped_session(Session)
    for key, val in country_data.items():
        name = val["name"]
        region = val["regionName"]
        lat = val["latitude"]
        lon = val["longitude"]
        aqi = val["aqi"]
        pm10 = val["pm10"]
        areas_count = val["areas"]
        curr_country = Country(
            name=name,
            region=region,
            latitude=float(lat),
            longitude=float(lon),
            aqi=float(aqi),
            pm10=float(pm10),
            areasCount=areas_count,
        )
        if Country.query.filter_by(name=name).count() == 0:
            s.add(curr_country)
            s.commit()
    s.close()

animal_images = {}
area_images = {}
country_images = {}

def get_animal_images():
    print("getting animal images...")
    s = scoped_session(Session)
    headers = {"Ocp-Apim-Subscription-Key": BING_API_KEY1}
    
    animals = Animal.query.all()
    
    for animal in animals:
        url_list = []
        name = animal.species
        parameters = {"q":name, "count":'10', "offset":'0', "mkt":'en-us', "safeSearch":'Moderate'}
        res = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/images/search?", headers=headers, params=parameters)
        search_res = res.json()

        animal_obj = Animal.query.filter_by(species=name).first()
        try:
            session_animal = Session.object_session(animal_obj)
            if session_animal is not None:
                session_animal.close()
        except:
            pass

        # if no results with species name, try common name
        if len(search_res["value"]) == 0:
            if animal_obj.name is not None:
                name = animal_obj.name
                parameters = {"q":name, "count":'10', "offset":'0', "mkt":'en-us', "safeSearch":'Moderate'}
                res = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/images/search?", headers=headers, params=parameters)
                search_res = res.json()
                animal_obj = Animal.query.filter_by(name=name).first()
                try:
                    session_animal = Session.object_session(animal_obj)
                    if session_animal is not None:
                        session_animal.close()
                except:
                    pass
                #print("name: ", name)
                #print("search res: ", search_res)
                if len(search_res["value"]) == 0:
                    animal_obj.image_one = 'no image available'
                    animal_obj.image_two = 'no image available'
                    animal_obj.image_three = 'no image available'
                    s.add(animal_obj)
                    s.commit()
                    continue
            else:
                animal_obj.image_one = 'no image available'
                animal_obj.image_two = 'no image available'
                animal_obj.image_three = 'no image available'
                s.add(animal_obj)
                s.commit()
                continue
        
        if len(search_res["value"]) >= 3:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]
            
            img2_res = search_res["value"][1]
            img2_url = img2_res["contentUrl"]

            img3_res = search_res["value"][2]
            img3_url = img3_res["contentUrl"]

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)

            animal_images[name] = url_list

            try:
                animal_obj.image_one = img1_url
                animal_obj.image_two = img2_url
                animal_obj.image_three = img3_url
                s.add(animal_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        elif len(search_res["value"]) >= 2:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]
            
            img2_res = search_res["value"][1]
            img2_url = img2_res["contentUrl"]

            img3_url = 'no image available'

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)

            animal_images[name] = url_list

            try:
                animal_obj.image_one = img1_url
                animal_obj.image_two = img2_url
                animal_obj.image_three = img3_url
                s.add(animal_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        elif len(search_res["value"]) >= 1:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]
            
            img2_url = 'no image available'

            img3_url = 'no image available'

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)

            animal_images[name] = url_list
            
            try:
                animal_obj.image_one = img1_url
                animal_obj.image_two = img2_url
                animal_obj.image_three = img3_url
                s.add(animal_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        else:
            animal_obj.image_one = 'no image available'
            animal_obj.image_two = 'no image available'
            animal_obj.image_three = 'no image available'
            s.add(animal_obj)
            s.commit()
        s.close()


    # get 2 images

def get_country_images():
    print("getting country images...")
    s = scoped_session(Session)
    headers = {"Ocp-Apim-Subscription-Key": BING_API_KEY1}
    
    countries = Country.query.all()
    for country in countries:
        url_list = []
        name = country.name
        flag_q = f"{name} flag"
        parameters = {"q":flag_q, "count":'10', "offset":'0', "mkt":'en-us', "safeSearch":'Moderate'}
        flag_res = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/images/search?", headers=headers, params=parameters).json()
        img_res = flag_res["value"][0]
        flag_url = img_res["contentUrl"]
        
        map_q = f"{name} in map"
        parameters = {"q":map_q, "count":'10', "offset":'0', "mkt":'en-us', "safeSearch":'Moderate'}
        map_res = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/images/search?", headers=headers, params=parameters).json()
        img_res = map_res["value"][0]
        map_url = img_res["contentUrl"]
        
        #print(name)
        url_list.append(flag_url)
        url_list.append(map_url)

        country_images[name] = url_list
        tcountry_obj = Country.query.filter_by(name=country).first()
        try:
            session_country = Session.object_session(country_obj)
            if session_country is not None:
                session_country.close()
        except:
            pass
        print("country obj: ", country_obj)
        
        country_obj.flag_image = url_list[0]
        
        country_obj.map_image = url_list[1]
        s.add(country_obj)
        s.commit()
        s.close()

def get_area_images():
    print("getting area images...")
    s = scoped_session(Session)
    headers = {"Ocp-Apim-Subscription-Key": BING_API_KEY1}
    
    areas = Area.query.all()
    
    for area in areas:
        url_list = []
        name = area.name
        area_q = f"{name} landscape"
        parameters = {"q":name, "count":'10', "offset":'0', "mkt":'en-us', "safeSearch":'Moderate'}
        res = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/images/search?", headers=headers, params=parameters)
        search_res = res.json()
        if(res.status_code != 200):
            print(res.status_code)
        area_obj = Area.query.filter_by(name=name).first()
        try:
            session_area = Session.object_session(area_obj)
            if session_area is not None:
                session_area.close()
        except:
            pass

        # get at least 3 images, using 2 in case some images are trash
        if len(search_res["value"]) >= 3:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]
            
            img2_res = search_res["value"][1]
            img2_url = img2_res["contentUrl"]
            
            img3_res = search_res["value"][2]
            img3_url = img3_res["contentUrl"]

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)
            area_images[name] = url_list
            
            try:
                area_obj.image_one = img1_url
                area_obj.image_two = img2_url
                area_obj.image_three = img3_url
                s.add(area_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        elif len(search_res["value"]) >= 2:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]
            
            img2_res = search_res["value"][1]
            img2_url = img2_res["contentUrl"]

            img3_url = 'no image available'

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)
            area_images[name] = url_list
            
            try:
                area_obj.image_one = img1_url
                area_obj.image_two = img2_url
                area_obj.image_three = img3_url
                s.add(area_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        elif len(search_res["value"]) >= 1:
            img1_res = search_res["value"][0]
            img1_url = img1_res["contentUrl"]

            img2_url = 'no image available'

            img3_url = 'no image available'

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)
            area_images[name] = url_list
            
            try:
                area_obj.image_one = img1_url
                area_obj.image_two = img2_url
                area_obj.image_three = img3_url
                s.add(area_obj)
                s.commit()
            except:
                print("this dont work")
                pass
        else:
            img1_url = 'no image available'

            img2_url = 'no image available'

            img3_url = 'no image available'

            url_list.append(img1_url)
            url_list.append(img2_url)
            url_list.append(img3_url)
            area_images[name] = url_list
            
            try:
                area_obj.image_one = img1_url
                area_obj.image_two = img2_url
                area_obj.image_three = img3_url
                s.add(area_obj)
                s.commit()
            except:
                print("this dont work")
                pass
    s.close()

country_images = {'Afghanistan': ['https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Afghanistan_Flag.jpg/1200px-Afghanistan_Flag.jpg', 'http://adst.org/wp-content/uploads/2015/02/Afghanistan-map-1024x847.jpg'], 'Albania': ['http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Albania-Flag-5.png', 'http://www.facingthestreet.com/wp-content/uploads/2011/01/Albania-map-canstockphoto3113769.jpg'], 'Andorra': ['https://www.worldatlas.com/webimage/flags/countrys/zzzflags/adlarge.gif', 'https://www.ezilon.com/maps/images/europe/Andorra-plotical-map.gif'], 'Armenia': ['http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Armenia-Flag-1.jpeg', 'https://eurasiangeopolitics.files.wordpress.com/2014/08/armenian-political-map.gif'], 'American Samoa': ['https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Flag_of_American_Samoa.svg/1200px-Flag_of_American_Samoa.svg.png', 'https://www.worldatlas.com/img/areamap/nonav/asau.gif'], 'Australia': ['http://cdn.wonderfulengineering.com/wp-content/uploads/2015/07/Australia-Flag-28.jpg', 'http://1.bp.blogspot.com/-6wv3-_jwUm8/T32nseJgUkI/AAAAAAAAAyg/2-EY4bdGiiA/s1600/Australia-politic-map.jpg'], 'Austria': ['https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/1200px-Flag_of_Austria.svg.png', 'http://www.orangesmile.com/common/img_country_maps/austria-map-0.jpg'], 'Azerbaijan': ['https://i.ytimg.com/vi/ijiXq9ibokQ/maxresdefault.jpg', 'https://www.nationsonline.org/maps/Azerbaijan-Map.jpg'], 'Belgium': ['https://i.ytimg.com/vi/qsVaNjWdGX0/maxresdefault.jpg', 'http://i.infopls.com/images/mbelgium.gif'], 'Bangladesh': ['https://cdn.britannica.com/67/6267-004-D59711CA.jpg', 'http://geology.com/world/bangladesh-map.gif'], 'Cyprus': ['https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/1920px-Flag_of_Cyprus.svg.png', 'https://www.justaboutcyprus.com/wp-content/uploads/2019/03/cyprus-political-map-1600.jpg'], 'Czech Republic': ['https://cdn.britannica.com/86/7886-004-297F88A4.jpg', 'https://blog.foreigners.cz/wp-content/uploads/2015/07/map-czech-republic.jpg'], 'Germany': ['https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1200px-Flag_of_Germany.svg.png', 'http://www.maps-of-europe.net/maps/maps-of-germany/detailed-administrative-map-of-germany.jpg'], 'Denmark': ['https://upload.wikimedia.org/wikipedia/commons/9/96/Flag_of_Denmark_ubt.jpeg', 'http://www.roughguides.com/wp-content/uploads/2015/04/area-map-of-Denmark.png'], 'Spain': ['http://1.bp.blogspot.com/-WGNY7WQmLTM/Tc2scs_RZ2I/AAAAAAAAA5I/zevPacstH8Y/s1600/Graphhics+National+Flag+of+Spanish+%25282%2529.jpg', 'https://www.roughguides.com/wp-content/uploads/2015/05/area-map-of-spain.png'], 'Estonia': ['https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Flag_of_Estonia.svg/2000px-Flag_of_Estonia.svg.png', 'https://www.nationsonline.org/maps/estonia-map.jpg'], 'Finland': ['https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/1024px-Flag_of_Finland.svg.png', 'https://www.roughguides.com/wp-content/uploads/2015/04/area-map-of-Finland-1.png'], 'Fiji': ['https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Fiji.svg/1200px-Flag_of_Fiji.svg.png', 'https://www.beautifulworld.com/wp-content/uploads/2016/10/fiji-oceania-map.jpg'], 'France': ['http://1.bp.blogspot.com/-_nCCCMxPDCo/Tchr7ZYrJPI/AAAAAAAAAwg/r6wf_cmCk-E/s1600/Wallpapers+Flag+of+France.jpg', 'http://tinyurl.com/map-of-france-regions'], 'Faroe Islands': ['https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Flag_of_the_Faroe_Islands.svg/1200px-Flag_of_the_Faroe_Islands.svg.png', 'http://suzzstravels.com/wp-content/uploads/2015/01/Faroe-Islands-Map.png'], 'Micronesia, Fed. Sts.': ['http://mecometer.com/image/infographic/micronesia/business-environment-legal-procedure.png', 'http://mecometer.com/image/infographic/micronesia/population-statistics.png'], 'United Kingdom': ['https://2.bp.blogspot.com/-P5qOt2KcR10/T0y7yn0sTWI/AAAAAAAADAg/7trepQzvN1M/s1600/1_National_Flag_of_United_Kingdom.gif', 'https://geology.com/world/united-kingdom-map.gif'], 'Georgia': ['https://www.washingtonpost.com/resizer/UhyS2V-0MJqL9e1eHcvr_ca-VLs=/1484x0/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/G3VSTGZIA5G4BHQZEOUIKNS4SM.JPG', 'https://www.fotolip.com/wp-content/uploads/2016/05/Political-Map-of-Georgia-7.gif'], 'Greece': ['https://i.ytimg.com/vi/Xqzd5P9wzBM/maxresdefault.jpg', 'https://fthmb.tqn.com/4JrVrD67gX_52fnk-0sm0XW2fw0=/960x0/filters:no_upscale()/GettyImages-150355158-58fb8f803df78ca15947f4f7.jpg'], 'Greenland': ['http://weekendroady.files.wordpress.com/2013/01/greenlandflag.gif', 'https://robbyrobinsjourney.files.wordpress.com/2016/01/greenland__map.jpg'], 'Guam': ['https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Flag_of_Guam.svg/1200px-Flag_of_Guam.svg.png', 'http://www.operationworld.org/files/ow/maps/lginset/guam-LMAP-md.png'], 'Croatia': ['http://justfunfacts.com/wp-content/uploads/2016/02/croatia-flag.jpg', 'http://4.bp.blogspot.com/-G0bhlqnH5Kc/UoIgszfqJSI/AAAAAAAAITc/1ZrpYwN6ILE/s1600/map_of_croatia.jpg'], 'Indonesia': ['https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/1200px-Flag_of_Indonesia.svg.png', 'https://www.nationsonline.org/maps/Indonesia-map.jpg'], 'Korea, Rep.': ['https://cdn.pixabay.com/photo/2016/01/06/02/51/julia-roberts-1123541_960_720.jpg', 'http://www.fitfortravel.nhs.uk/images/malariamaps/southkorea/southkorea.gif'], 'Lao PDR': ['http://www.aseanbriefing.com/news/wp-content/uploads/2015/10/Lao-PDR-flag.png', 'https://greeningthegrid.org/site-images/lao-pdr-wind-map'], 'Sri Lanka': ['https://www.graphicmaps.com/r/w1047/images/flags/lk-flag.jpg', 'https://www.ezilon.com/maps/images/asia/Srilanka-political-map.gif'], 'Maldives': ['https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Flag_of_Maldives.svg/1200px-Flag_of_Maldives.svg.png', 'http://4.bp.blogspot.com/-KG2bOOJWUzg/T_s2FKuO38I/AAAAAAAAGok/ol5TKTKYrMo/s1600/maldives-map-1.jpg'], 'Marshall Islands': ['https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Flag_of_the_Marshall_Islands.svg/1200px-Flag_of_the_Marshall_Islands.svg.png', 'https://www.worldatlas.com/img/areamap/nonav/mhau.gif'], 'Myanmar': ['https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/1200px-Flag_of_Myanmar.svg.png', 'http://www.operationworld.org/files/ow/maps/lgmap/myan-MMAP-md.png'], 'Mongolia': ['https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Flag_of_Mongolia.svg/1200px-Flag_of_Mongolia.svg.png', 'https://www.amicusmongolia.com/images/maps/mongolia-map-large.jpg'], 'Northern Mariana Islands': ['http://www.worldatlas.com/webimage/flags/countrys/zzzflags/cqlarge.gif', 'https://scubadivingresource.com/wp-content/uploads/2014/05/karte-3-817.gif'], 'Malaysia': ['https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Flag_of_Malaysia.svg/1200px-Flag_of_Malaysia.svg.png', 'http://www.expatgo.com/my/wp-content/uploads/2015/09/Political-Map-of-Malaysia.jpg'], 'New Caledonia': ['https://www.graphicmaps.com/r/w1047/images/flags/nc-flag.jpg', 'http://blogs.oregonstate.edu/gemmlab/files/2015/09/new-caledonia-map.gif'], 'Nepal': ['https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Flag_of_Nepal.svg/1200px-Flag_of_Nepal.svg.png', 'https://www.beautifulworld.com/wp-content/uploads/2018/05/Where-is-Kathmandu.jpg'], 'Nauru': ['https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Flag_of_Nauru.svg/1200px-Flag_of_Nauru.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Nauru_map_english.svg/1200px-Nauru_map_english.svg.png'], 'New Zealand': ['http://media3.s-nbcnews.com/j/newscms/2015_33/1165256/150810-new-zealand-flag-jpo-419a_4f74ffd03fafd40c3c8681257db5da42.nbcnews-ux-2880-1000.jpg', 'http://www.tourism.net.nz/images/maps/worldmap-lg.jpg'], 'Pakistan': ['https://i.ytimg.com/vi/GkaeskWmSII/maxresdefault.jpg', 'https://geology.com/world/pakistan-map.gif'], 'Philippines': ['http://i.ebayimg.com/images/i/191813969538-0-1/s-l1000.jpg', 'http://geocurrents.info/wp-content/uploads/2013/09/Philippines-Regions-Map.png'], 'Palau': ['https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Palau.svg/1200px-Flag_of_Palau.svg.png', 'https://www.worldatlas.com/img/areamap/nonav/pwnewz.gif'], 'Papua New Guinea': ['https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Papua_New_Guinea.svg/1920px-Flag_of_Papua_New_Guinea.svg.png', 'https://paigewest.files.wordpress.com/2012/05/political-map-of-papguinea.gif'], 'Korea, Dem. People’s Rep.': ["https://www.mapsofworld.com/flags/images/world-flags/Democratic-People'S-Republic-of-Korea-flag.jpg", 'http://img.static.reliefweb.int/sites/reliefweb.int/files/resources-pdf-previews/121844-Map-Flood-Affected-Areas-July-28-2012.png'], 'United States': ['http://www.123countries.com/wp-content/uploads/2015/03/United-States-Flag-Image.jpg', 'http://img.wikinut.com/img/1-ed4ya9f_hzomy3/jpeg/0/United-States-Map.jpeg']}

"""
for country in country_images:
    url_list = country_images[country]
    s = scoped_session(Session)
    country_obj = Country.query.filter_by(name=country).first()
    try:
        session_country = Session.object_session(country_obj)
        if session_country is not None:
            session_country.close()
    except:
        pass
    print("country obj: ", country_obj)
    
    country_obj.flag_image = url_list[0]
    
    country_obj.map_image = url_list[1]
    s.add(country_obj)
    s.commit()
s.close()
"""

#get_country_images()

#with open('country_imgs.txt', 'w') as file:
#     file.write(str(country_images))

#get_animal_images()
#get_area_images()

"""
with open('animals_imgs.txt', 'w') as file:
     file.write(str(animal_images))

with open('areas_imgs.txt', 'w') as file:
     file.write(str(area_images))

"""

#get_list_countries()

#print("Gathered data from APIs.")
#print("Populating database...")

"""
with open('countries.txt', 'w') as file:
     file.write(str(country_data))

with open('animals.txt', 'w') as file:
     file.write(str(animal_data))

with open('areas.txt', 'w') as file:
     file.write(str(area_data))
"""
#populate_country_table()
#populate_animals_table()
#populate_area_table()
print("Populating data done.")
