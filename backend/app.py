import flask
from flask import request, jsonify
from flask_cors import CORS
from controllers import app

# TODO: fix strings in logitude and latitude, not right format for python
# TODO: change the route of the get calls to our database

CORS(app)
app.config["DEBUG"] = True

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
