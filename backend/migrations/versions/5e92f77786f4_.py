"""empty message

Revision ID: 5e92f77786f4
Revises: 4aed0390dd1b
Create Date: 2019-11-14 21:07:33.060798

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5e92f77786f4'
down_revision = '4aed0390dd1b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('animal', sa.Column('image_three', sa.String(length=500), nullable=True))
    op.add_column('country', sa.Column('flag_image', sa.String(length=500), nullable=True))
    op.add_column('country', sa.Column('map_image', sa.String(length=500), nullable=True))
    op.drop_column('country', 'image_two')
    op.drop_column('country', 'image_one')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('country', sa.Column('image_one', sa.VARCHAR(length=500), autoincrement=False, nullable=True))
    op.add_column('country', sa.Column('image_two', sa.VARCHAR(length=500), autoincrement=False, nullable=True))
    op.drop_column('country', 'map_image')
    op.drop_column('country', 'flag_image')
    op.drop_column('animal', 'image_three')
    # ### end Alembic commands ###
