import React from 'react'
import './shared.css'
import EcuadorFlag from './images/EcuadorFlag.png';
import EcuadorMap from './images/EcuadorMap.gif';
import EcuadorSatellite from './images/EcuadorSatellite.jpg';
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Ecuador = () => (
<header class="header">
    <h1>Ecuador</h1>
    <div class="row">
        <div class="col">
            <img src={EcuadorFlag} class="resize" alt="logo" />
        </div>
        <div class="col">
            <img src={EcuadorMap} class="resize" alt="logo" />
        </div>
        <div class="col">
            <img src={EcuadorSatellite} class="resize" alt="logo" />
        </div>
    </div>
   <p>Latitude: 31.8312° S </p>
   <p>Longitude: 78.1834° W</p>
   <p>Number of Protected Areas: 83 </p>
   <p>Air Quality Level: 34 </p>
   <p>Atmospheric Pressure: 759.81 </p>
    <Link to='/countries'>
        <img src={backIcon} class="link-icon" alt="logo"/>
    </Link>
</header>
)

export default Ecuador