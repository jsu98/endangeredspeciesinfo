import React from 'react'
import Helmet from 'react-helmet';
import pangolin from './images/pangolin.jpg';
import bengalTiger from './images/bengalTiger.jpg';
import sriLankanElephant from './images/sriLankanElephant.jpg';
import mountainGorilla from './images/mountainGorilla.jpg';
import javanRhino from './images/javanRhino.jpg';
import './Home.css'
import './shared.css'

const Home = () => (
  <div>
    <Helmet>
        <title>Endangered Animals Home</title>
    </Helmet>
    <header className="header">
      <h1>
        {/* <img src={pangolin} className="Home-logo rounded-circle" alt="Endangered Animals logo" /> */}
        Endangered Animals
      </h1>

      <div id="animals" class="carousel slide" data-ride="carousel">
        <ul class="carousel-indicators">
          <li data-target="#animals" data-slide-to="0" class="active"></li>
          <li data-target="#animals" data-slide-to="1"></li>
          <li data-target="#animals" data-slide-to="2"></li>
          <li data-target="#animals" data-slide-to="3"></li>
          <li data-target="#animals" data-slide-to="4"></li>
        </ul>

        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src={pangolin} className="Home-logo rounded-circle" alt="Pangolin"/>
          </div>
          <div class="carousel-item">
            <img src={bengalTiger} className="Home-logo rounded-circle" alt="Bengal Tiger"/>
          </div>
          <div class="carousel-item">
            <img src={sriLankanElephant} className="Home-logo rounded-circle" alt="Sri Lankan Elephant"/>
          </div>
          <div class="carousel-item">
            <img src={mountainGorilla} className="Home-logo rounded-circle" alt="Mountain Gorilla"/>
          </div>
          <div class="carousel-item">
            <img src={javanRhino} className="Home-logo rounded-circle" alt="Javan Rhino"/>
          </div>
        </div>

        <a class="carousel-control-prev" href="#animals" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#animals" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>

      <a
        className="Home-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
      </a>
    </header>
  </div>
)

export default Home
