import React from 'react'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

class AreaPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          items: [],
          isLoaded: false,
        };
      };
      
      componentDidMount() {
        var id = this.props.match.params.name;
        fetch(`https://endangeredanimals.me/api/area?q={"filters":[{"name":"name","op":"eq","val":"${id}"}]}`,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
              })
          })
      };

  render() {
    if (!this.state.isLoaded){
        return <header class = "header" > <div> Loading....</div></header>
    }
    else{
    return (
      <header class="header">
        <Helmet>
            <title>{this.state.items[0].name}</title>
        </Helmet>

        <h1>Name: {this.state.items[0].name}</h1>
        <p>Size (km^2): {this.state.items[0].area}</p>
        <p>Status Date: {this.state.items[0].lastUpdated}</p>
        <p>WDPA ID: {this.state.items[0].wdpa_id}</p>
        <p>Country: {this.state.items[0].country}</p>
      
        <Link to='/protectedareas'>
            <img src={backIcon} class="link-icon" alt="Go back to Protected Areas page"/>
        </Link>
      </header>
    );
    }
  }
};




export default AreaPage;