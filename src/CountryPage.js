import React from 'react'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

class CountryPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          items: [],
          isLoaded: false,
        };
      };
      
      componentDidMount() {
        var id = this.props.match.params.name;
        fetch(`https://endangeredanimals.me/api/country?q={"filters":[{"name":"name","op":"eq","val":"${id}"}]}`,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
              })
          })
      };

  render() {
    if (!this.state.isLoaded){
        return <header class = "header" > <div> Loading....</div></header>
    }
    else{
    return (
      <header class="header">
        <Helmet>
            <title>{this.state.items[0].name}</title>
        </Helmet>

        <h1>Name: {this.state.items[0].name}</h1>
        <p>AQI: {this.state.items[0].aqi}</p>
        <p>Latitude: {this.state.items[0].latitude}</p>
        <p>Longitude: {this.state.items[0].longitude}</p>
        <p>Region: {this.state.items[0].region}</p>
      
        <Link to='/countries'>
            <img src={backIcon} class="link-icon" alt="Go back to Countries page"/>
        </Link>
      </header>
    );
    }
  }
};




export default CountryPage;