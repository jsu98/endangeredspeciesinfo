import React from 'react'
import './shared.css'
import ferretTwo from './images/ferretTwo.jpg';
import ferretEndangered from './images/ferretEndangered.png';
import ferretMap from './images/ferretMap.png';
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Ferret = () => (
<header class="header">
    <h1>Black-footed Ferret</h1>
    <div class="row">
        <div class="col">
            <img src={ferretTwo} class="resize" alt="USA flag" />
        </div>
        <div class="col">
            <img src={ferretEndangered} class="resize" alt="USA map" />
        </div>
        <div class="col">
            <img src={ferretMap} class="resize" alt="USA satellite image" />
        </div>
    </div>
    <p>Species: Mustela nigripes</p>
    <p>Habitat: Prairie</p>
    <p>Country: USA</p>
    <p>Population Size: 1,000</p>
    <Link to='/Animals'>
        <img src={backIcon} class="link-icon" alt="Go back to Countries page"/>
    </Link>
</header>
)

export default Ferret