import React from 'react'
import { Link } from 'react-router-dom'

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
  <div className="allign">
    <nav class="navbar navbar-expand-sm bg-light navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item active"><Link class="nav-link" to='/'>Home</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/countries'>Countries</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/protectedareas'>Protected Areas</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/animals'>Animals</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/about'>About</Link></li>
      </ul>
    </nav>
  </div>
)

export default Header
