import React from 'react'
import './shared.css'
import USAFLAG from './images/USAFLAG.png';
import usMap from './images/usMap.gif';
import usSat from './images/usSat.jpg';
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const USA = () => (
<header class="header">
    <h1>USA</h1>
    <div class="row">
        <div class="col">
            <img src={USAFLAG} class="resize" alt="USA flag" />
        </div>
        <div class="col">
            <img src={usMap} class="resize" alt="USA map" />
        </div>
        <div class="col">
            <img src={usSat} class="resize" alt="USA satellite image" />
        </div>
    </div>
    <p>Latitude: 37.0902° N </p>
    <p>Longitude: 95.7129° W</p>
    <p>Number of Protected Areas: 25,800 </p>
    <p>Air Quality Level: 139 </p>
    <p>Atmospheric Pressure: 760 mm Hg </p>
    <Link to='/countries'>
        <img src={backIcon} class="link-icon" alt="Go back to Countries page"/>
    </Link>
</header>
)

export default USA