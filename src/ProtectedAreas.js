import React from 'react'
import Helmet from 'react-helmet'
import './shared.css'
import yasuni from './images/yasuni.jpg';
import { Link } from 'react-router-dom'

/*
Database info needs to get here somehow, not sure how that works
Once we have database info, to build the grid you need to get info 3 instances at a time and map them to AnimalRows
*/

class ProtectedAreas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
      numPerPage: 10,
      totalPages: 0,
      currentPage: 1,
      search: '',
      searchTerms: [],
      filteredSearch: [],
      sortField: "none",
      direction: "asc",
      filterField: "none",
      filter: ""
    };
  };
  handleClick(page){
    this.state.currentPage=page
    this.runQuery()
}
  handleField(field){
        this.state.currentPage=1
    this.state.sortField=field
    this.runQuery()
}
  handleDirection(direction){
        this.state.currentPage=1
    this.state.direction=direction
    this.runQuery()
}
  handleFilterField(field){
        this.state.currentPage=1
    this.state.filterField=field
    this.runQuery()
}
  handleFilter(filter){
        this.state.currentPage=1
    this.state.filter=filter
    this.runQuery()
}

updateSearch(event){
    this.state.search = event.target.value.toLowerCase()
    this.runQuery()
}

handleFilter(event){
    this.state.filter=event.target.value
    this.runQuery()
}

runQuery() {
    var query = `https://endangeredanimals.me/api/area?page=${this.state.currentPage}`
    if(this.state.search != "") {
        query += `&q={"filters":[{"or": [{"name":"name","op":"ilike","val":"%${this.state.search}%"},
        {"name":"country","op":"ilike","val":"%${this.state.search}%"}]}`
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `,{"name":"${this.state.filterField}","op":"eq","val":"${this.state.filter}"}]`
        } else {
            query += "]"
        }
        if(this.state.sortField != "none") {
            query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
        } else {
            query += "}"
        }
    } else {
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `&q={"filters":[{"name":"${this.state.filterField}","op":"eq","val":"${this.state.filter}"}]`
            if(this.state.sortField != "none") {
                query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            } else {
                query += "}"
            }
        } else {
            if(this.state.sortField != "none") {
                query += `&q={"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            }
        }
    }
        fetch(query,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
                  totalPages: json.total_pages,
                  currentPage: json.page,   
              })
          })
}
  
  componentDidMount() {
    fetch('https://endangeredanimals.me/api/area',
    {  mode: 'cors'})
      .then(res => res.json())
      .then(json => {
          this.setState({
              isLoaded: true,
              items: json.objects,
              totalPages: json.total_pages,
              currentPage: json.page,
              search: '',
              filteredSearch: this.items,
          })
      })
  };
  


  render() {
      var {isLoaded, items, numPerPage, totalPages, currentPage, filteredSearch} = this.state;
     if(items.length >= 1) {
        filteredSearch = items.filter((protectedArea) => {  if(protectedArea.name) 
            return (protectedArea.name.toLowerCase().indexOf(this.state.search) != -1  || 
            protectedArea.area && protectedArea.area.toString().indexOf(this.state.search) != -1    ||
            protectedArea.lastUpdated && protectedArea.lastUpdated.toString().indexOf(this.state.search) != -1    ||
            protectedArea.wdpa_id && protectedArea.wdpa_id.toString().indexOf(this.state.search) != -1    ||
            protectedArea.country && protectedArea.country.toLowerCase().indexOf(this.state.search) != -1)              
        }) 
    } else {
        filteredSearch = []
    }

      return (
        <header class="header">
        <Helmet>
            <title>Countries</title>
        </Helmet>
        <ul class="pagination">
   {currentPage > 1 ? <li onClick={this.handleClick.bind(this, currentPage-1)} class="page-item"><a class="page-link" href="#">Previous</a></li>
   : <li class="disabled page-item"><a class="page-link" href="#">Previous</a></li>
   }
  {currentPage > 1 && <li onClick = {this.handleClick.bind(this, currentPage-1)}class="page-item"><a class="page-link" href="#">{currentPage - 1 }</a></li>}
  <li class="active page-item"><a class="page-link" href="#">{currentPage}</a></li>
  {currentPage != totalPages && <li onClick ={this.handleClick.bind(this, currentPage+1)} class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>}
 {currentPage != totalPages ?
  <li onClick ={this.handleClick.bind(this, currentPage+1)}class="page-item "><a class="page-link" href="#">Next</a></li>
  : <li class="disabled page-item"><a class="page-link" href="#">Next</a></li>
  }
</ul>

<div class="md-form mt-0">
  <input class="form-control" type="text" value = {this.state.search} onChange = {this.updateSearch.bind(this)} placeholder="Search" aria-label="Search"/>
</div>


           <div class= "row">
            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Sort
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleField.bind(this, "area")} class="dropdown-item" href="#">Size</a>
                <a onClick={this.handleField.bind(this, "lastUpdated")} class="dropdown-item" href="#">Status Date</a>
                <a onClick={this.handleField.bind(this, "wdpa_id")} class="dropdown-item" href="#">WDPA ID</a>
                <a onClick={this.handleField.bind(this, "country")} class="dropdown-item" href="#">Country</a>
                <a onClick={this.handleField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-1">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Order
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleDirection.bind(this, "asc")} class="dropdown-item" href="#">Ascending</a>
                <a onClick={this.handleDirection.bind(this, "desc")} class="dropdown-item" href="#">Descending</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Filter
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleFilterField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleFilterField.bind(this, "area")} class="dropdown-item" href="#">Size</a>
                <a onClick={this.handleFilterField.bind(this, "lastUpdated")} class="dropdown-item" href="#">Status Date</a>
                <a onClick={this.handleFilterField.bind(this, "wdpa_id")} class="dropdown-item" href="#">WDPA ID</a>
                <a onClick={this.handleFilterField.bind(this, "country")} class="dropdown-item" href="#">Country</a>
                <a onClick={this.handleFilterField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
                <div class="md-form mt-0">
                    <input class="form-control" type="text" value = {this.state.filter} onChange = {this.handleFilter.bind(this)} placeholder="Filter Value" aria-label="Filter Value"/>
                </div>
            </div>

            </div>

           <div class= "row">
            {filteredSearch.map(instance =>
                   <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top" src={yasuni} alt="Card image cap"/>
                     <div class="card-body">
                    {instance.name.toLowerCase().indexOf(this.state.search) != -1 && this.state.search != '' ?
                    <Link to={`/area/${instance.name}`}><h5 class="card-title text-dark">
                      {instance.name.substring(0, instance.name.toLowerCase().indexOf(this.state.search))}
                      <mark>
                        {(instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search), instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length) )}
                      </mark>
                      {instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.name.length)}
                    </h5></Link>:
                    <Link to={`/area/${instance.name}`}>
                        <h5 class="card-title text-dark">{instance.name}</h5></Link>}
                     </div>
                    <ul class="list-group list-group-flush">
                        {instance.area && instance.area.toString().indexOf(this.state.search) != -1 && this.state.search != '' ?<li class="list-group-item text-dark">Size (km^2): {instance.area.substring(0, instance.area.toString().indexOf(this.state.search))}<mark>{(instance.area.substring(instance.area.toString().indexOf(this.state.search), instance.area.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.area.substring(instance.area.toString().indexOf(this.state.search) + this.state.search.length, instance.area.length)}</li>:
                        <li class="list-group-item text-dark">Size (km^2): {instance.area}</li>}
                        {instance.lastUpdated && instance.lastUpdated.toString().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Status Date: {instance.lastUpdated.substring(0, instance.lastUpdated.toString().indexOf(this.state.search))}<mark>{(instance.lastUpdated.substring(instance.lastUpdated.toString().indexOf(this.state.search), instance.lastUpdated.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.lastUpdated.substring(instance.lastUpdated.toString().indexOf(this.state.search) + this.state.search.length, instance.lastUpdated.length)}</li>:
                        <li class="list-group-item text-dark">Status Date: {instance.lastUpdated}</li>}
                        {instance.wdpa_id && instance.wdpa_id.toString().indexOf(this.state.search) != -1 &&  this.state.search != ''?<li class="list-group-item text-dark">WDPA ID: {instance.wdpa_id.substring(0, instance.wdpa_id.toString().indexOf(this.state.search))}<mark>{(instance.wdpa_id.substring(instance.wdpa_id.toString().indexOf(this.state.search), instance.wdpa_id.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.wdpa_id.substring(instance.wdpa_id.toString().indexOf(this.state.search) + this.state.search.length, instance.wdpa_id.length)}</li>:
                        <li class="list-group-item text-dark">WDPA ID: {instance.wdpa_id}</li>}
                        {instance.country && instance.country.toLowerCase().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Country: {instance.country.substring(0, instance.country.toLowerCase().indexOf(this.state.search))}<mark>{(instance.country.substring(instance.country.toLowerCase().indexOf(this.state.search), instance.country.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.country.substring(instance.country.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.country.length)}</li>:
                        <li class="list-group-item text-dark">Country: {instance.country}</li>}
                        {/* {instance.area.toLowerCase().indexOf(this.state.search) != -1 ?<li class="list-group-item text-dark">{instance.taxonId.substring(0, instance.taxonId.indexOf(this.state.search))}<mark>{(instance.taxonId.substring(instance.taxonId.indexOf(this.state.search), instance.taxonId.indexOf(this.state.search) + this.state.search.length))}</mark>{instance.taxonId.substring(instance.taxonId.indexOf(this.state.search) + this.state.search.length, instance.taxonId.length)}</li>:
                        <li class="list-group-item text-dark">Taxon Id = {instance.taxonId}</li>} */}
                    </ul>
                <div class="card-body">
            
                </div>
                    </div>
                    </div>
                )
            }
        </div>
        </header>
        )  
    };
}



export default ProtectedAreas

