import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './home'
import Animals from './Animals'
import ProtectedAreas from './ProtectedAreas'
import About from './About'
import Countries from './Countries'
import USA from './USA'
import Malaysia from './Malaysia'
import Ecuador from './Ecuador'
import Ferret from './ferret'
import Otter from './Otter'
import Tapir from './Tapir'
import Yellowstone from './Yellowstone'
import Yasuni from './Yasuni'
import Penang from './Penang'
import AnimalPage from './AnimalPage'
import CountryPage from './CountryPage'
import AreaPage from './AreaPage'
// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = (props) => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/animals' component={Animals}/>
      <Route path='/protectedareas' component={ProtectedAreas}/>
      <Route path='/about' render={() => <About commitCount={props.commitCount} issueCount={props.issueCount}/>}/>
      <Route path='/countries' component={Countries}/>
      <Route path='/usa' component={USA}/>
      <Route path='/malaysia' component={Malaysia}/>
      <Route path='/Ecuador' component={Ecuador}/>
      <Route path='/ferret' component={Ferret}/>
      <Route path='/Otter' component={Otter}/>
      <Route path='/Tapir' component={Tapir}/>
      <Route path='/yellowstone-national-park' component={Yellowstone}/>
      <Route path='/yasuni-national-park' component={Yasuni}/>
      <Route path='/penang-national-park' component={Penang}/>
      <Route path="/animal/:name" component={AnimalPage} />
      <Route path="/country/:name" component={CountryPage} />
      <Route path="/area/:name" component={AreaPage} />
    </Switch>
  </main>
);

export default Main
