import React from 'react'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

/*
An AnimalPage takes in an animalId when it is routed to, based on this id the attributes should be filled in from the database.
You could also use names directly as the animalId, just change the url
*/

class AnimalPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          items: [],
          isLoaded: false,
        };
      };
      
      componentDidMount() {
        var id = this.props.match.params.name;
        fetch(`https://endangeredanimals.me/api/animal?q={"filters":[{"name":"name","op":"eq","val":"${id}"}]}`,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
              })
          })
      };

  render() {
    // const { params: { animalId } } = this.props;
    if (!this.state.isLoaded){
        return <header class = "header" > <div> Loading....</div></header>
    }
    else{
    return (
      <header class="header">
        <Helmet>
            <title>{this.state.items[0].name}</title>
        </Helmet>

        <h1>Name: {this.state.items[0].name}</h1>
        <p>Habitat: {this.state.items[0].habitat}</p>
        <p>Status: {this.state.items[0].status}</p>
        <p>Population: {this.state.items[0].population}</p>
        <p>Species: {this.state.items[0].species}</p>
      
        <Link to='/animals'>
            <img src={backIcon} class="link-icon" alt="Go back to Animals page"/>
        </Link>
      </header>
    );
    }
  }
};




export default AnimalPage;