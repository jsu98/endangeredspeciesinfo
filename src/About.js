import React from 'react'
import Helmet from 'react-helmet'
import Adriana from './images/AdrianaSWE.jpeg'
import Nikhil from './images/NikhilSWE.jpg'
import Marcus from './images/MarcusSWE.png'
import David from './images/DavidSWE.jpg'
import Jack from './images/JackSWE.jpg'
import './shared.css'
import './About.css';
import Table from 'react-bootstrap/Table'


const About = (props) => (
  <header class="header">
    <div>
      <Helmet>
          <title>About</title>
      </Helmet>
      <h1>About</h1>
      <p >
        Welcome to EndangeredAnimals.me! 
        This website is designed to analyze data regarding endangered species,
        countries where these species reside, and relevant protected areas 
        in order to better inform the general public of human impact
        on the status of these species.
      </p>

      <h1>Results</h1>
        <p>Our website uses data from the following models: 
        Endangered species, Country, and Protected Area
        Although these models are disparate, information from these models
        help understand the status of endangered species through correlation. 
        For example, countries with lower air quality might have more
        endangered species due to pollution. More protected areas in
        a country could mean more species can avoid endangered status,
        or might conversely mean that species that would otherwise go 
        extinct could stay endangered due to conservation efforts.
      </p>

       <div class= "row">
                     
                  <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top h-10" src={Nikhil} alt="Card image cap"/>
                     <div class="card-body">
                    <h5 class="card-title text-dark">
                      Nikhil Bodicharla</h5>
                     </div>
                    <ul class="list-group list-group-flush ">
                       <li class="list-group-item text-dark"> Bio: Ball is life</li>
                       <li class="list-group-item text-dark"> Frontend</li>
                       <li class="list-group-item text-dark"> Commits: {props.commitCount[0]}</li>
                       <li class="list-group-item text-dark"> Issues: {props.issueCount[0]}</li>
                       <li class="list-group-item text-dark"> Unit Tests = 0</li>
                    </ul>
                <div class="card-body">
                </div>
                    </div>
                  </div>

                  <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top h-10" src={Adriana} alt="Card image cap"/>
                     <div class="card-body">
                    <h5 class="card-title text-dark">
                      Adriana Ixba</h5>
                     </div>
                    <ul class="list-group list-group-flush ">
                       <li class="list-group-item  text-dark"> I love outreach, sports and music</li>
                       <li class="list-group-item text-dark"> Backend</li>
                       <li class="list-group-item text-dark"> Commits: {props.commitCount[1]}</li>
                       <li class="list-group-item text-dark"> Issues: {props.issueCount[1]}</li>
                       <li class="list-group-item text-dark"> Unit Tests = 0</li>
                    </ul>
                <div class="card-body">
                </div>
                    </div>
                  </div>

                  <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top h-10" src={Marcus} alt="Card image cap"/>
                     <div class="card-body">
                    <h5 class="card-title text-dark">
                      Marcus Mao</h5>
                     </div>
                    <ul class="list-group list-group-flush ">
                       <li class="list-group-item text-dark"> Bio: Just trying to pass this class</li>
                       <li class="list-group-item text-dark"> Frontend</li>
                       <li class="list-group-item text-dark"> Commits: {props.commitCount[2]}</li>
                       <li class="list-group-item text-dark"> Issues: {props.issueCount[2]}</li>
                       <li class="list-group-item text-dark"> Unit Tests = 0</li>
                    </ul>
                <div class="card-body">
                </div>
                    </div>
                  </div>

                  <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top" src={Jack} alt="Card image cap"/>
                     <div class="card-body">
                    <h5 class="card-title text-dark">
                      Jack Su</h5>
                     </div>
                    <ul class="list-group list-group-flush ">
                       <li class="list-group-item text-dark"> Bio: My hobbies are procrastina</li>
                       <li class="list-group-item text-dark"> Backend</li>
                       <li class="list-group-item text-dark"> Commits: {props.commitCount[4]}</li>
                       <li class="list-group-item text-dark"> Issues: {props.issueCount[4]}</li>
                       <li class="list-group-item text-dark"> Unit Tests = 0</li>
                    </ul>
                <div class="card-body">
                </div>
                    </div>
                  </div>
                
            
        </div>



<h2>Stats</h2>
<Table striped bordered hover variant="dark" >
  <thead>
    <tr>
      <th>Name</th>
      <th>Commits</th>
      <th>Issues</th>
      <th>Unit Tests</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Nikhil Bodicharla</td>
      <td>{props.commitCount[0]}</td>
      <td>{props.issueCount[0]}</td>
      <td>0</td>
    </tr>
    <tr>
      <td>Adriana Ixba</td>
      <td>{props.commitCount[1]}</td>
      <td>{props.issueCount[1]}</td>
      <td>0</td>
    </tr>
    <tr>
      <td>Marcus Mao</td>
      <td>{props.commitCount[2]}</td>
      <td>{props.issueCount[2]}</td>
      <td>0</td>
    </tr>
    <tr>
      <td>Jack Su</td>
      <td>{props.commitCount[4]}</td>
      <td>{props.issueCount[4]}</td>
      <td>0</td>
    </tr>
  </tbody>
</Table>
  
        <h3>Links to Data Sources </h3>
        <p><a href="https://documenter.getpostman.com/view/8989826/SVtR1VQk?version=latest"> RESTful API documentation  </a></p>
        <p><a href="https://gitlab.com/marsunmao/endangeredspeciesinfo">GitLab Repo</a></p>
        <p><a href="https://www.protectedplanet.net/c/world-database-on-protected-areas"> Protected Planet API: Protected Areas Info</a></p>
        <p>Protected planet keeps track of protected environmental areas across the globe, and various statistics about them.</p>
        <p>We obtain various information such as location (lattitude, longitude, country), size in square miles, and founding date.</p>
        <p><a href="https://apiv3.iucnredlist.org/api/v3/docs">IUCN Red List API: Endangered Species Info</a></p>
        <p>IUCN Red List keeps track of various endangered species around the world.</p>
        <p>We use this database in order to display the status of endangered species, their primary habitat(s),</p>
        <p>and remaining population.</p>
        <p><a href="https://datahelpdesk.worldbank.org/knowledgebase/articles/898590-country-api-queries">Country-specific Info</a></p>
        <p>The Worldbank Database contains a variety of different databases, but we primarily use it to collect</p>
        <p>basic information on countries around the world in order to link countries to species and protected areas.</p>

        <h4>{"\n"}Tools Used</h4>
          <p><b>Postman</b></p>
          <p>Tool used for testing API calls as well as designing our RESTful API.</p>
  
          <p><b>Amazon AWS </b></p>
          <p>Backend tool for hosting our website.</p>
   
      
          <p><b>GitLab</b></p>
          <p>Git repository service that we update with our website's code.</p>
    
      
          <p><b>React</b></p>
          <p>Javascript library used for website development</p>
  
    </div>
  </header>
)

export default About
