import React from 'react'
import './shared.css'
import penang from './images/penang.jpg'
import penangMap from './images/penangMap.png'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Penang = () => (
<header class="header">
    <h1>Penang National Park</h1>
    <div class="row">
        <div class="col">
            <img src={penang} class="resize" alt="Penang"/>
        </div>
        <div class="col">
            <img src={penangMap} class="resize" alt="Penang map"/>
        </div>
    </div>
    <p>Latitude: 5°26'N </p>
    <p>Longitude: 100°11'E</p>
    <p>Country: Malaysia</p>
    <p>Area: 9.9 sq mi</p>
    <p>Established: April 10th, 2003</p>
    <Link to='/protectedareas'>
        <img src={backIcon} class="link-icon" alt="Go back to Protected Areas page"/>
    </Link>
</header>
)

export default Penang